#pragma once

#include <dazzle.h>
#include <gtksourceview/gtksource.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_EDITOR_SEARCH_BAR (example_editor_search_bar_get_type())


G_DECLARE_FINAL_TYPE (ExampleEditorSearchBar, example_editor_search_bar, EXAMPLE, EDITOR_SEARCH_BAR, DzlBin)


GtkSearchEntry *example_editor_search_bar_get_search       (ExampleEditorSearchBar *self);
void             example_editor_search_bar_set_search       (ExampleEditorSearchBar *self,
                                                         GtkSearchEntry    *search);
gboolean         example_editor_search_bar_get_show_options (ExampleEditorSearchBar *self);
void             example_editor_search_bar_set_show_options (ExampleEditorSearchBar *self,
                                                         gboolean            show_options);
gboolean         example_editor_search_bar_get_replace_mode (ExampleEditorSearchBar *self);
void             example_editor_search_bar_set_replace_mode (ExampleEditorSearchBar *self,
                                                         gboolean            replace_mode);

G_END_DECLS

